import React, { Component } from "react";

class Form extends Component {
  state = {
    account: {
        firstName:"",
        lastName:""
    },
  };

  handleChange = e => {
    const account = { ...this.state.account };
    account[e.currentTarget.name] = e.currentTarget.value;
    this.setState({account})
  };

  handleSubmit = e => {
    e.preventDefault();
    alert(`Hi ${this.state.account.firstName} ${this.state.account.lastName}`);
  };

  render() {
    return (
      <form className="row" onSubmit={this.handleSubmit}>
        <input
          value={this.state.account.firstName}
          name="firstName"
          onChange={this.handleChange}
          placeholder="First Name"
        />
        <input
          value={this.state.account.lastName}
          name="lastName"
          onChange={this.handleChange}
          placeholder="Last Name"
        />
        <button className="btn btn-primary">Go</button>
        <label>
          {this.state.account.firstName} {this.state.account.lastName}
        </label>
      </form>
    );
  }
}

export default Form;
